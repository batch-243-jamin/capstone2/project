const mongoose = require('mongoose');

const checkoutSchema = new mongoose.Schema({
	
		userId: { 
					type: String, 
					required: [ true, "UserId is required"]
				}, 

		productId: 
				{ 
					type: String, 
					required: [ true, "ProductId is required"]
				}, 

		quantity:
				{ 
					type: Number, 
					required: [ true, "quantity is required"]
				}, 

		totalAmount:
				{ 
					type: Number, 
					required: [ true, "totalAmount is required"]
				}, 
		purchasedOn: 
				{
					type: Date,
					default: new Date()
				}				

});
module.exports = mongoose.model("Checkout", checkoutSchema);