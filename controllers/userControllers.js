const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const Checkout = require("../models/Checkout");

const auth = require("../auth");

module.exports.checkEmailExists = (request, response, next) =>{
	// The result is sent back to the frontend via the then method
	return User.find({email:request.body.email}).then(result =>{
		console.log(request.body.email);
		let message = ``;
			// find method returns an array record of matching documents
		if(result.length >0){
			message = `The ${request.body.email} is already taken, please use other email.`
			return response.send(message);
		}
			// No duplicate email found
			// The email is not yet registered in the database.
		else{
			next();
		}
	})
}

module.exports.registerUser = (request, response) =>{

	// creates variable "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request body to provide the necessary information.
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		/*salt - salt rounds that bcrypt algortihm will run to encrypt the password*/
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	});

	// Saves the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		response.send(`Congratulations, ${newUser.email}!. You are now registered.`)
	}).catch(error =>{
		console.log(error);
		response.send(`Sorry ${newUser.email}, there was an error during the registration. Please try again!`)
	})
}

module.exports.loginUser = (request, response) =>{
	// The findOne method, returns the first record in the collection that matches the search criteria.

	return User.findOne({email : request.body.email})
	.then(result =>{

		console.log(result);

		if(result === null){
			response.send(`Your email: ${request.body.email}, is not yet registered. Register first!`);
		}
		else{
					// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
					// The compareSync method is used to compare a non encrytpted password from the login from to the encrypted password retrieve. It will return true or false value depending on the result.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				let token = auth.createAccessToken(result);
				console.log(token);
				return response.send({accessToken: token});
			}
			else{
				return response.send(`Incorrect password, please try again!`);
			}
		}
	})
}

module.exports.getProfile = (request, response) =>{

	return User.findById(request.body.id).then(result => {
		result.password = "******";
		console.log(result.order);
		console.log(result);
		return response.send(result);
	}).catch(error => {
		console.log(error);
		return response.send(error);
	})
}

module.exports.getUserOrder = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);

	return User.findById(userData.id).then(result => {
		result.password = "******";
		console.log(result);
	}).then(result => console.log(request.body.order)).catch(error => {
		console.log(error);
		return response.send(error);
	})
}

module.exports.updateRole = (request, response) => {
	let token = request.headers.authorization;
	let userData = auth.decode(token);

	let idToBeUpdated = request.params.userId;

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result => {
			let update = {
				isAdmin: !result.isAdmin
			};

		return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
			document.password = "Confindential";
		 response.send(document)}).catch(err => response.send(err));
		}).catch(err => response.send(err));
	}else{
		return response.send("You don't have access on this page!")
	}
}

// Checkout

module.exports.addToCart = async (request, response) => {
	const productId = request.params.productId;
	const quantity = request.body.quantity
	const token = request.headers.authorization;
	let userData = auth.decode(token);
	console.log(userData);

	if(!userData.isAdmin){
		let data = {
			productId: productId,
			userId: userData.id
		}
		
		console.log(data);
	let isProductUpdated = await Product.findById(data.productId).then(result => {
		console.log(result);
		result.customers.push({
			userId: data.userId
			})
			console.log(result);
			result.stocks -= quantity;

			return result.save().then(success => {
				return true;
			}).catch(err => { console.log(err); return false});
		}).catch(err => {
			console.log(err);
			return false;
		});
		let totalAmount = await Product.findById(data.productId).then(result => result);

		let isUserUpdated = await User.findById(data.userId).then(result => {
		console.log(result);
		result.order.push({
			
				productId: data.productId, 
				quantity: quantity,
				totalAmount: totalAmount.price*quantity})

		return result.save().then(success => {
				console.log(success); return true;
			}).catch(err => { console.log(err); return false})
		}).catch(err => {console.log(err); return false});
		console.log(isProductUpdated);
		console.log(isUserUpdated);
		let Order = (isUserUpdated && isProductUpdated) ? response.send("Thank you for purchasing!") : response.send("We encountered an error in your order, please try again!")

	}
	else{
		response.send("You are admin, you cannot purchase.")
	}
}


// Checkout product
module.exports.Checkout = (request, response) =>{
	const productId = request.params.productId;
	const token = request.headers.authorization;
	let userData = auth.decode(token);

	if(!userData.isAdmin){
		return User.findById(userData.id).then(result => {
			let newCheckout = new Checkout(
					{
						userId: userData.id,
						productId: productId,
						quantity: request.body.quantity,
						totalAmount: request.body.totalAmount
					}
				)
			console.log(newCheckout);
			return newCheckout.save().then(saved => {
				console.log(saved);
				response.send(saved);
			}).catch(error => {
				console.log(error);
				response.send("You got an error.");
			})
		})
	}
	else{
		return response.send("You don't have an access.")
	}

}

// total of carts
module.exports.checkOutTotal = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	
	if(!userData.isAdmin){
		return Checkout.find({userId: userData.id}).then(result => {
			return Checkout.aggregate([
					{$match: {
						userId: userData.id
					}},
					{$group: {
						_id: "$userId",
						TotalPrice: {$sum: "$totalAmount"}
					}}
				]).then(total =>{
					response.send(total)
					console.log(total);
				}) .catch(error => {
					console.log(error);
				}).catch(error => {
					console.log(error);
				})
			})
	}else{
		return response.send("You don't have an access.");
	}
}
