const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");
	// Route for checking Email
	router.post("/checkEmail", userController.checkEmailExists);

	// route for registration
	router.post("/register",  userController.checkEmailExists, userController.registerUser);
	
	// route for log in
	router.post("/login", userController.loginUser);

	// Get user
	router.post("/details", auth.verify ,userController.getProfile);

	// Get user order
	router.post("/userOrder",auth.verify, userController.getUserOrder);

	// Total amount
	router.get("/checkoutTotal", auth.verify, userController.checkOutTotal);


	// update a role
	router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

	// addToCart
	router.post("/order/:productId", auth.verify, userController.addToCart);

	// Checkout
	router.post("/checkout/:productId", auth.verify, userController.Checkout);



module.exports = router;